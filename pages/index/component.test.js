describe('pages/index/index', () => {
	let page
	beforeAll(async () => {
		// 重新reLaunch至首页，并获取首页page对象（其中 program 是uni-automator自动注入的全局对象）
		page = await program.reLaunch('/pages/index/index')
		await page.waitFor(1000)
	})

	it('title', async () => {
		// 检测首页u-link的文本内容
		expect(await (await page.$('.title')).text()).toBe(
			'Hello')
	})

})