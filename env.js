module.exports = {
    "is-custom-runtime": false,
    "compile": true,
    "h5": {
        "options": {
            "headless": true
        },
        "executablePath": ""
    },
    "mp-weixin": {
        "port": 9420,
        "account": "",
        "args": "",
        "cwd": "",
        "launch": true,
        "teardown": "disconnect",
        "remote": false,
        "executablePath": "D:\\Program Files\\微信web开发者工具\\cli.bat"
    },
    "app-plus": {
        "android": {
            "id": "",
            "executablePath": ""
        },
        "version": "",
        "ios": {
            "id": "",
            "executablePath": ""
        }
    }
}
