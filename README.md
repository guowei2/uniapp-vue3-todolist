# uniapp-vue3-todolist

uniapp hbuildx版todolist

## 开发体验

问题：

1. hbuildx编辑器没有dom静态闭合检查，dom未正确闭合时没有给出提示
2. hbuildx不支持持续集成，需要转成cli项目才支持 <https://uniapp.dcloud.net.cn/worktile/CI.html>

## 开发注意

1. 全局安装czg ,之后提交都用czg命令，这样更规范
