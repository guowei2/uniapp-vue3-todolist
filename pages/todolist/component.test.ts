describe('pages/index/index', () => {
  let page
  beforeAll(async () => {
    // 重新reLaunch至首页，并获取首页page对象（其中 program 是uni-automator自动注入的全局对象）
    page = await program.reLaunch('/pages/todolist/index')
    await page.waitFor('.new-todo')
  })

  it('visits the todolist', async () => {
    const expectText = await page.$('.todoapp>.header>.h1').then((el) => el.text())
    expect(expectText).toBe('Todos')
  })

  it('add todo', async () => {
    const text = 'add 1'
    page
      .$('.new-todo')
      .then((el) => el.input(text))
      .catch((e) => {
        // 当前input会抛出一个e.stopPropagation is not a function异常，但是不影响正常赋值
        console.log('input', e.toString())
      })
    await page.waitFor('.add-btn')
    await page.$('.add-btn').then((el) => el.tap())
    const last = await page.$$('.todo-list>.todo').then((els) => els[els.length - 1])
    const expectText = await last.$('.view>label').then((el) => el.text())
    expect(expectText).toBe(text)
  })

  it('edit todo', async () => {
    const editText = 'edit text'
    const last = await page.$$('.todo-list>.todo').then((els) => els[els.length - 1])
    await last.$('.view>label').then((el) => el.longpress())
    await page.waitFor(() =>
      page.$$('.todo-list>.todo').then((els) => els[els.length - 1].$('.edit'))
    )
    await last
      .$('.edit')
      .then((el) => el.input(editText))
      .catch((e) => {
        // 当前input会抛出一个e.stopPropagation is not a function异常，但是不影响正常赋值
        console.log('input', e.toString())
      })
    await page.waitFor(() =>
      page.$$('.todo-list>.todo').then((els) => els[els.length - 1].$('.confirm'))
    )
    await last.$('.confirm').then((el) => el.tap())
    const expectText = await last.$('.view>label').then((el) => el.text())
    expect(expectText).toBe(editText)
  })

  it('edit todo esc', async () => {
    const last = await page.$$('.todo-list>.todo').then((els) => els[els.length - 1])
    const oldText = await last.$('.view>label').then((el) => el.text())
    await last.$('.view>label').then((el) => el.longpress())
    await page.waitFor(() =>
      page.$$('.todo-list>.todo').then((els) => els[els.length - 1].$('.edit'))
    )
    await last
      .$('.edit')
      .then((el) => el.input('will cancel text'))
      .catch((e) => {
        // 当前input会抛出一个e.stopPropagation is not a function异常，但是不影响正常赋值
        console.log('input', e.toString())
      })
    await page.waitFor(() =>
      page.$$('.todo-list>.todo').then((els) => els[els.length - 1].$('.confirm'))
    )
    await last.$('.cancel').then((el) => el.tap())
    const expectText = await last.$('.view>label').then((el) => el.text())
    expect(expectText).toBe(oldText)
  })

  it('delete todo', async () => {
    const LengthBefore = await page.$$('.todo-list>.todo').then((els) => els.length)
    const last = await page.$$('.todo-list>.todo').then((els) => els[els.length - 1])
    await last.$('.destroy').then((el) => el.tap())
    const LengthAfter = await page.$$('.todo-list>.todo').then((els) => els.length)
    expect(LengthBefore - LengthAfter).toBe(1)
  })
})
